# qserver

* Original author: Peter Selinger, adapted by Frank Fu as a server for dpq.

* To install: `stack install`. This should generate the executable `qserver`.
  
* To run: `qserver -d` to start the server daemon.


